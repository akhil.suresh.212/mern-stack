const mongoose = require("../db");
const ObjectId = mongoose.ObjectId;

todoSchema = mongoose.Schema(
  {
    userId: {
      type: ObjectId,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    desc: {
      type: String,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("todo", todoSchema);
